const { Router } = require('express')
const { getUsers, puttUser, postUser, deleterUser } = require('../controllers/users.controller')

const router = Router()

router.get('/', getUsers)
router.put('/:id', puttUser)
router.post('/', postUser)
router.delete('/', deleterUser)

module.exports = router