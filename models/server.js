const express = require('express')
var cors = require('cors')

class Server {
  constructor() {
    this.app = express()
    this.port = process.env.PORT;
    this.midelwares();
    this.routes();
  }

  midelwares() {
    this.app.use(express.static('public'))
    this.app.use(cors())
    this.app.use(express.json())
  }
  routes() {
    this.app.use('/api/users', require('../routes/user.routes'))
  }
  listen() {
    this.app.listen(this.port, () => {
      console.log("server starter", this.port)
    })
  }
}
module.exports = Server