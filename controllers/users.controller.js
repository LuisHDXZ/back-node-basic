
const { response } = require('express')

const getUsers = (req, res = response) => {
  const query = req.query;

  res.json({
    msg: 'get controllers',
    query
  })
}

const puttUser = (req, res) => {
  const id = req.params.id

  res.json({
    msg: 'put api',
    id
  })
}
const postUser = (req, res) => {
  const body = req.body;
  console.log("--body", body)
  res.json({
    msg: body
  })
}

const deleterUser = (req, res) => {
  res.json({
    msg: 'delete api'
  })
}


module.exports = {
  getUsers,
  deleterUser,
  postUser,
  puttUser
}